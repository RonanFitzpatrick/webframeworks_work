/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.ca2;


//import ie.dkit.webframeworks.controllers.Login;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author ronan
 */
public class FrameworkTest
{
   ;
    protected String urlStem;
    WebClient webClient = new WebClient();
    public FrameworkTest()
    {
         urlStem = "localhost:8080/";
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
         
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp() throws IOException
    {
        
    }
    
    @After
    public void tearDown()
    {
    }
    
    @Ignore
    public void findPages()
    {   //error due to classpath ?
        
        //assertEquals(true,store.getPage("/login").getClass().isInstance(new Login()));
    }
    
    private String fullUrlForPath(String path) 
    {
        return urlStem + path;
    }
    
    @Ignore
    public void ProductPageWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("Product?product=Fallout_4"));
        Assert.assertTrue(page.getBody().asText().contains("Fallout 4"));
    }
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
