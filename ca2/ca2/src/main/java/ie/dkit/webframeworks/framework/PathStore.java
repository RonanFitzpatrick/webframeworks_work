/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.framework;
import java.awt.List;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;

/**
 *
 * @author ronan
 */
public class PathStore
{

    //String refers to the path object is the class that deals with that path.
    public HashMap<String, Class> store;
    
    
    public PathStore() throws IOException
    {
        store = new HashMap<String, Class>();
        populateStore();
    }

    public Class getPage(String path)
    {
        if (store.containsKey(path))
        {
            return store.get(path);
        }
        return null;
    }

    private void populateStore() throws IOException
    {

        Class<webPage> annotation = webPage.class;

        //change to read in  path from file
         Properties properties = new Properties();
        

        properties.load(this.getClass().getResourceAsStream("/site.properties"));
        
       
        String path = properties.getProperty("properties.projectPath");
        
        Reflections reflections = new Reflections(ClasspathHelper.forPackage(path));

        //Used to get all the files with the correct annotation so they can be
        //redirected to given the  correct path names. This seemed the most 
        //flexible and readable method to do this.
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(annotation);

        for (Class annotated_class : annotated)
        {
            

            webPage page = (webPage) annotated_class.getAnnotation(annotation);
            store.put(page.url(), annotated_class);

        }

    }

    @Override
    public String toString()
    {
        return "PathStore{" + "store=" + store   + '}';
    }

}
