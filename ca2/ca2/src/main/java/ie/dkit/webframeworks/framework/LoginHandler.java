/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.context.WebContext;

/**
 *
 * @author ronan
 */
public class LoginHandler extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String userName = request.getParameter("userName");
        String password = request.getParameter("userpass");
        
        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream("/users.txt"));
        BufferedReader br = new BufferedReader(reader);
        try
        {
            String line;
            while ((line = br.readLine()) != null)
            {   
                
                
                if(line.equals(userName+":"+password))
                {
                    //set cookie return to main page
                    Cookie user = new Cookie("username",userName);
                    response.addCookie(user);
                    ServletContext servletContext = request.getServletContext();
                    WebContext webContext = new WebContext(request, response, servletContext, request.getLocale());
                    PathStore store = new  PathStore();
                    HttpServlet servlet = (HttpServlet) store.getPage("/Home").newInstance();
                    servlet.service(request, response);
                    
                }
                else
                {
                    
                }

            }
        } catch (Exception e)
        {

        }
        //response.getWriter().print();

    
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
