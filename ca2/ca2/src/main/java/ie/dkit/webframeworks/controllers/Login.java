/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.controllers;


import ie.dkit.webframeworks.framework.Templater;
import ie.dkit.webframeworks.framework.webPage;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.context.WebContext;

/**
 *
 * @author ronan
 */
@webPage(url = "/Login")
public class Login extends HttpServlet
{
   


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        
        ServletContext servletContext = request.getServletContext();
        WebContext webContext = new WebContext(request, response, servletContext, request.getLocale());
        Templater.template("Login", response,webContext);
        

    }
    
    
        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
                               throws IOException, ServletException 
    {

               
                        //read file and set pairs as arraylist 
                        
                        // Creating Servlet Context object
                        
                        //check if the username and password
                               
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}