/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.controllers;

/**
 *
 * @author ronan
 */
public class Game
{
    private final String picture_url;
    private final double price;
    private final String name;
    private final String short_desctription;
    private String url = "/Product?product=";

    @Override
    public String toString()
    {
        return "Game{" + "picture_url=" + picture_url + ", price=" + price + ", name=" + name + ", short_desctription=" + short_desctription + '}';
    }

   

    public Game(String picture_url, String name, String short_desctription,double price)
    {
        this.picture_url = picture_url;
        this.price = price;
        this.name = name;
        this.short_desctription = short_desctription;
        this.url += name.replaceAll(" ","_");
    }

    public String getUrl()
    {
        return url;
    }

    public String getPicture_Url()
    {
        return picture_url;
    }

    public double getPrice()
    {
        return price;
    }

    public String getName()
    {
        return name;
    }

    public String getShort_desctription()
    {
        return short_desctription;
    }

    
    
}
