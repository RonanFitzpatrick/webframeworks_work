/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.framework;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

/**
 *
 * @author ronan
 */
public class Templater
{
    //templates the page passed to it and provides the variable of time/date
    public static void template(String path,HttpServletResponse response,WebContext webContext) throws IOException
    {
       ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
       TemplateEngine templateEngine = new TemplateEngine();
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheable(false); 
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        Calendar cal = Calendar.getInstance();
        webContext.setVariable("today", dateFormat.format(cal.getTime()));
        
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.process(path, webContext, response.getWriter());
   
    }
    
}
