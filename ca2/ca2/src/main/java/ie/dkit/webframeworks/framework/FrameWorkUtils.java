/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.framework;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

/**
 *
 * @author ronan
 */
public class FrameWorkUtils
{
    
    public static void template(String path,HttpServletResponse response,WebContext webContext) throws IOException
    {
       ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
       TemplateEngine templateEngine = new TemplateEngine();
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheable(false); 
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        Calendar cal = Calendar.getInstance();
        webContext.setVariable("today", dateFormat.format(cal.getTime()));
        
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.process(path, webContext, response.getWriter());
   
    }
    
    public static String login_option(HttpServletRequest request)
    {
        
        
        
    	Cookie[] cookies = request.getCookies();
    
        if(cookies != null)
        {
     
    	for(Cookie cookie : cookies)
        {
                
    		if(cookie.getName().equals("username"))
                {
                    return cookie.getValue();
                }
        
        }
        
    }
        return null;
    }
    
    public static void logout(HttpServletRequest request)
    {
        
        
        
    	Cookie[] cookies = request.getCookies();
    
        if(cookies != null)
        {
     
    	for(Cookie cookie : cookies)
        {
                
    		if(cookie.getName().equals("username"))
                {
                    cookie.setValue(null);
                }
        
        }
        
    }
        
    }
    
}
