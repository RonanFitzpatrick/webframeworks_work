/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.controllers;

import ie.dkit.webframeworks.framework.FrameWorkUtils;
import ie.dkit.webframeworks.framework.Templater;
import ie.dkit.webframeworks.framework.webPage;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.context.WebContext;

/**
 *
 * @author ronan
 */


@webPage(url = "/Product")
public class Product extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    private static final ArrayList<Game> game_list = new ArrayList();
    
    /*  
    This should be read from a database or a file etc but for now I will
        instantiate the products myself through the use of objects.
    */
    static
    {
        game_list.add(new Game("/resources/Fallout.jpg", "Fallout 4", "An rpg game "
                + "based in a post apocalyptic world", 67.99));
        game_list.add(new Game("/resources/justCause.jpg", "Just cause 3", "open world"
                + " action-adventure video game developed by Avalanche Studios "
                + "and published by Square Enix", 72.45));
        game_list.add(new Game("/resources/callofduty.jpg", "Call of duty black ops 3",
                "A scientific millitary first person shooter developed by activision"
                , 54.45));
    }



    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        ServletContext servletContext = request.getServletContext();
        WebContext webContext = new WebContext(request, response, servletContext, request.getLocale());
        
        
       
         
         String user = FrameWorkUtils.login_option(request);
         //a sepearte jave class should set these things and return webContext object
         //as it is done in two places
         //logged in
        if(user != null)
        {
        webContext.setVariable("name", "Hello "+user);
        webContext.setVariable("login_menu", "Logout");
        webContext.setVariable("login_path", "/Logout");
        }
        //not logged in
        else
        {
        webContext.setVariable("name", "");
        webContext.setVariable("login_menu", "Login");
        webContext.setVariable("login_path", "/Login");
        }
        
        for(Game game :game_list)
        {
            //should be a hashmap but it wouldnt matter as much if games were in a database
            if(game.getName().replaceAll(" ", "_").equals(request.getParameter("product")))
            {
                webContext.setVariable("current_game", game);
            }
        }
        
        Templater.template("Product", response,webContext);
        

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
