/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.framework;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author ronan
 */

//annotation that allows the pages and their path to be found
@Retention(RetentionPolicy.RUNTIME)
public @interface webPage
{
    String url() default "/index";

}
