package ie.dkit.webframeworks.ca1;

public class Person {
    
    private String firstname;
    private String surname;
    
    public Person(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
    }
    
    public void setFirstnameAndSurname() {
        this.firstname = "John";
        this.surname = "Doe";
    }
    
    public void setFirstnameAndSurname(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname; 
    }
    
    public String getFullname() {
        return String.format("%s, %s", surname, firstname);
    }
    
}
