package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;

public interface Dispatcher
{

    public void setinterpreter(interpreter interpreter);

    public String dispatchCommand(String command) throws MalformedURLException, CommandDispatchException;

}
