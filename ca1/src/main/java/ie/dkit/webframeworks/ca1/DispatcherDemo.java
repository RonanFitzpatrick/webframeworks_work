/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;
import java.util.Scanner;

/**
 *
 * @author ronan
 */
public class DispatcherDemo
{

    public static void main(String[] args) throws ClassNotFoundException, MethodExecutionException,
            DuplicateObjectException, InvalidObjectNameException, NoSuchMethodException,
            ObjectNotFoundException, InstantiationException, IllegalAccessException,
            CommandDispatchException, MalformedURLException
    {
        //set up
        DispatcherImpl dis = new DispatcherImpl();
        Scanner keyboard = new Scanner(System.in);
        interpreterImpl inter = new interpreterImpl();
        dis.setinterpreter(inter);

        //first request
        System.out.println("Please Enter your command or -1 to exit");
        String input = keyboard.nextLine();

        //basic run loop
        while (!input.equals("-1"))
        {

            //the work
            try
            {

                System.out.println(dis.dispatchCommand(input));

            } catch (Exception e)
            {
                System.err.println(e);
                System.out.println("Something went wrong with the command you tried to execute try again.");

            }
            //update
            System.out.println("Please Enter your command or -1 to exit");
            input = keyboard.nextLine();

        }

    }

}
