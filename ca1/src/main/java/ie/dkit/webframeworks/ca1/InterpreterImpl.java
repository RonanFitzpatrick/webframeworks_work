/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.ca1;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author ronan
 */
public class interpreterImpl implements interpreter
{

    // PG: final as well??
    private HashMap<String, Object> store = new HashMap();

    @Override
    public void createObject(String name, String type) throws ClassNotFoundException, MethodExecutionException, DuplicateObjectException, InvalidObjectNameException, NoSuchMethodException
    {
        //PG: is this comment necessary / is it covering up a method name that could be better?
        //validation
        object_name_validation(name);

        try
        {
            //create
            store.put(name, Class.forName(type).newInstance());

        } catch (InstantiationException | IllegalAccessException ex)
        {
            Logger.getLogger(interpreterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void createObject(String name, String type, String[] args) throws ClassNotFoundException, MethodExecutionException, DuplicateObjectException, InvalidObjectNameException, NoSuchMethodException
    {
        //validation
        object_name_validation(name);

        ArrayList<Class> types = getTypes(args);

        try
        {
            //uses arguments to create a new object to add to map
            Object created_object = Class.forName(type)
                    .getConstructor(types.toArray(new Class[]
                                    {
                    })).newInstance((Object[]) args);

            store.put(name, created_object);

        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
        {
        }

    }

    @Override
    public void deleteObject(String name) throws ObjectNotFoundException
    {

        if (store.keySet().contains(name))
        {

            store.remove(name);

        }
    }

    @Override
    public String callMethod(String name, String methodName) throws ObjectNotFoundException, NoSuchMethodException, MethodExecutionException
    {
        Object object = store.get(name);

        try
        {

            return (String) object.getClass().getMethod(methodName).invoke(object);

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
        {
            Logger.getLogger(interpreterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return (String) object;
    }

    @Override
    public String callMethod(String name, String methodName, String[] args) throws ObjectNotFoundException, NoSuchMethodException, MethodExecutionException
    {

        ArrayList<Class> types = getTypes(args);

        Object object = store.get(name);

        try
        { //PG: good - states assumptions.
         //returns what the method named returns. This assumes the methods being
            //called returns String. Could check genericReturnType if program was
            //going to be extended to deal with other systems.
            return (String) object.getClass().getMethod(methodName, types.
                    toArray(new Class[]
                            {
                    })).invoke(object, (Object[]) args);

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
        {
            Logger.getLogger(interpreterImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "";
    }

    @Override
    public List<String> objectNamesOfType(String type)
    {
        ArrayList<String> matched_list = new ArrayList<>();

        //functional version of for each loop using a lambda instead of an if.
        store.keySet().stream().filter((key) -> (store.get(key).getClass().getName()
                .equals(type))).forEach((key) ->
                        {
                            matched_list.add(key);
                });

        return matched_list;

    }

    //PG: well-encapsulated function but could do with a better name.
    public void object_name_validation(String name) throws InvalidObjectNameException
    {
        if (name.equals("") || RegexUtility.matches(name, "\\d+")
                || !RegexUtility.matches(name, "[\\w]+") || name.equals("new")
                || name.equals("delete"))
        {
            throw new InvalidObjectNameException();
        }
    }

    @Override
    public List<String> objectNames()
    {
        ArrayList list = new ArrayList(Arrays.asList(store.keySet().toArray()));
        return list;

    }

    private ArrayList getTypes(String[] args)
    {
        ArrayList<Class> types = new ArrayList<>();
        for (int i = 0; i < args.length; i++)
        {

            types.add(args[i].getClass());
         //most common primitive types are covered.Can be extended to
            //cover more cases.
            if (types.get(i) == integer.class)
            {
                types.set(i, int.class);
            } else if (types.get(i) == Double.class)
            {
                types.set(i, double.class);
            } else if (types.get(i) == Boolean.class)
            {
                types.set(i, boolean.class);
            } else if (types.get(i) == Short.class)
            {
                types.set(i, short.class);
            } else if (types.get(i) == int.class)
            {
                types.set(i, int.class);
            }

        }
        return types;
    }

}
