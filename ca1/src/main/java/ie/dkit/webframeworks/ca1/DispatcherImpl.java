/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ronan
 */
public class DispatcherImpl implements Dispatcher
{

    private interpreter interpreter;

    @Override
    public void setinterpreter(interpreter interpreter)
    {
        this.interpreter = interpreter;

    }

    @Override
    public String dispatchCommand(String command) throws MalformedURLException, CommandDispatchException
    {

        try
        {
            return typeCheck(command);
            // PG: line length!! if they get very int consider judicious line breaking/continuation.
        } catch (ObjectNotFoundException | NoSuchMethodException | MalformedURLException | MethodExecutionException | ClassNotFoundException | DuplicateObjectException | InvalidObjectNameException ex)
        {
            throw new MalformedURLException();
        }

    }

    private String typeCheck(String command) throws MalformedURLException,
            ObjectNotFoundException, NoSuchMethodException,
            MethodExecutionException, ClassNotFoundException,
            DuplicateObjectException, InvalidObjectNameException
    {

        //Regexs to break down commands,more control  
        String create_regex = "\\/new\\/([\\w\\.]+)\\/([\\w]+)";
        String create_regex_param = create_regex + "\\/(.*)";
        String delete_regex = "\\/delete\\?name=([\\w]+)";
        String object_filter_regex = "\\/objects\\/([\\w\\.]+)";
        String method_call_regex = "\\/([\\w]+)\\/([\\w]+)";
        String method_call_param_regex = method_call_regex + "\\/(.*)";

        //PG: int if/else blocks often a candidate for command pattern-type dispatch
        //checks nd spilts for which methods to call
        if (RegexUtility.matches(command, create_regex))
        {
            interpreter.createObject(RegexUtility.getFirst(command, create_regex, 2),
                    RegexUtility.getFirst(command, create_regex, 1));
        } else if (RegexUtility.matches(command, create_regex_param))
        {

            interpreter.createObject(RegexUtility.getFirst(command, create_regex_param, 2),
                    RegexUtility.getFirst(command, create_regex_param, 1),
                    RegexUtility.getFirst(command, create_regex_param, 3).split("\\/"));
        } else if (RegexUtility.matches(command, delete_regex))
        {

            interpreter.deleteObject(RegexUtility.getFirst(command, delete_regex, 1));

        } else if (command.equals("/objects"))
        {

            return interpreter.objectNames().toString();

        } else if (RegexUtility.matches(command, object_filter_regex))
        {

            return interpreter.objectNamesOfType(RegexUtility.getFirst(command,
                    object_filter_regex, 1)).toString();

        } else if (RegexUtility.matches(command, method_call_regex))
        {

            String[] args =
            {
            };
            return method_call_parser(command, method_call_regex, args);

        } else if (RegexUtility.matches(command, method_call_param_regex))
        {

            return method_call_parser(command, method_call_regex, RegexUtility.getFirst(command,
                    method_call_param_regex, 3).split("\\/"));

        } else
        {

            throw new MalformedURLException();

        }
        //default return if no errors occur
        return "command complete";
    }

    private String method_call_parser(String command, String regex, String[] args) throws ObjectNotFoundException, NoSuchMethodException, MethodExecutionException
    {

        String name = RegexUtility.getFirst(command, regex, 1);
        String method = RegexUtility.getFirst(command, regex, 2);
        return interpreter.callMethod(name, method, args);

    }

}
