package ie.dkit.webframeworks.propertieslab;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/*
 In this exercise, subclass the PropertyReader and Override the get method
 so that it returns the string "don't know" for any property whose value is 
 unknown. 
 */
public class DefaultPropertyReader extends PropertyReader
{

    @Override
    public String getPropertyFromFile(String propertyName) throws IOException
    {
        // complete!
        Properties properties = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/site.properties");
        properties.load(fis);
        String s = properties.getProperty(propertyName, "don't know");
        
        return s;
    }

}
