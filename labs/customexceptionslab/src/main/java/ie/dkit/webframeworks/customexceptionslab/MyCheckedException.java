/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.customexceptionslab;

/**
 *
 * @author ronan
 */
public class MyCheckedException extends Exception
{

    public MyCheckedException()
    {
    }

    public MyCheckedException(String message)
    {
        super(message);
    }

    public MyCheckedException(Exception cause)
    {
        super(cause);
    }
    
}
