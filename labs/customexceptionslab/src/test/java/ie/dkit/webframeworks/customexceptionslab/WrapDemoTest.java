package ie.dkit.webframeworks.customexceptionslab;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

public class WrapDemoTest {
    
    private final String qualifiedName = "ie.dkit.webframeworks.customexceptionslab.WrapDemoImpl";
    private WrapDemo demo;
    
    @Before
    public void setup() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        demo = (WrapDemo) Class.forName(qualifiedName).newInstance();
    }
    
    @After
    public void teardown() {
        demo = null;
    }
    
    @Test
    public void isImplemented() throws Exception {
        Class.forName(qualifiedName).newInstance();
    }
    
    @Test
    public void throwsOuterException() throws Exception {
        try {
            demo.demonstrateWrapping();
            fail();
        } catch ( Exception e ) {
            assertEquals("ie.dkit.webframeworks.customexceptionslab.MyCheckedException", e.getClass().getName());
        }
    }
    
    @Test
    public void wrapsInnerExceptionOfCorrectType() {
        try {
            demo.demonstrateWrapping();
            
            // shouldn't get this far
            fail();
        } catch ( Exception e ) {
            assertEquals("ie.dkit.webframeworks.customexceptionslab.MyRuntimeException", e.getCause().getClass().getName());
        }
    }
    
    @Test
    public void wrapsInnerExceptionWithCorrectMessage() {
        try {
            demo.demonstrateWrapping();
            
            // shouldn't get this far
            fail();
        } catch ( Exception e ) {
            assertEquals("low level external problem", e.getCause().getMessage());
        }
    }
    
}
