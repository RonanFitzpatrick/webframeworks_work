package ie.dkit.webframeworks.customexceptionslab;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/* DO NOT MODIFY TEST CODE! */

public class MyRuntimeExceptionTest {
    
    private final String qualifiedName = "ie.dkit.webframeworks.customexceptionslab.MyRuntimeException";
    
    @Test
    public void shouldBeImplemented() {
        boolean available = false;
        try {
            Class.forName(qualifiedName);
            available = true;
        } catch (ClassNotFoundException e) {
            // swallow
        }
        assertTrue(available);
    }
    
    @Test
    public void shouldExtendRuntimeException() throws ClassNotFoundException {
        assertTrue(Class.forName(qualifiedName).getSuperclass().equals(RuntimeException.class));
    }
    
    @Test
    public void mustAllowEmptyConstructor() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, NoSuchMethodException, NoSuchMethodException, NoSuchMethodException {
        Class.forName(qualifiedName).newInstance();
    }
    
    @Test
    public void mustAllowStringArgumentConstructor() throws Exception {
        Class.forName(qualifiedName).getDeclaredConstructor(String.class).newInstance("hello");
    }
    
    @Test
    public void mustAllowExceptionArgumentConstructor() throws Exception {
        Exception wrapped = new Exception("this is the wrapped exception");
        Class.forName(qualifiedName).getDeclaredConstructor(Exception.class).newInstance(wrapped);
    }
    
    @Test
    public void stringArgumentMustBeMessage() throws Exception {
        String message = "message text here";
        Exception e = (Exception) Class.forName(qualifiedName).getDeclaredConstructor(String.class).newInstance(message);
        assertEquals(message, e.getMessage());
    }
    
    @Test
    public void exceptionArgumentMustBeWrapped() throws Exception {
        String message = "message text here";
        Exception wrapped = new Exception(message);
        Exception e = (Exception) Class.forName(qualifiedName).getDeclaredConstructor(Exception.class).newInstance(wrapped);
        assertEquals(wrapped, e.getCause());
    }
}

