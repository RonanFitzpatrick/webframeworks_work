package ie.dkit.webframeworks.servletslab;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Assert;
import org.junit.Test;

public class DemoServletIT {
    
    WebClient webClient = new WebClient();
    
    protected String urlStem;
    public DemoServletIT() {
        urlStem = "http://localhost:8180/servletslab/DemoServlet?";
    }
    
    private String fullUrlForPath(String path) {
        return urlStem + path;
    }
    
    @Test
    public void demoWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath(""));
        Assert.assertTrue(page.getBody().asText().contains("Servlet"));
    }
    
    
}
