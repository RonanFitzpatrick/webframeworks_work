package ie.dkit.webframeworks.templatinglab;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

public class MainServlet extends HttpServlet {
    
    private final ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
    private final TemplateEngine templateEngine = new TemplateEngine();
    
    @Override
    public void init() {
        
        // from: http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html#the-template-resolver
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheable(false); // true gives better performance but painful development!
        
        templateEngine.setTemplateResolver(templateResolver);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        
        // this gives us a holder to put variables into
        ServletContext servletContext = request.getServletContext();
        WebContext webContext = new WebContext(request, response, servletContext, request.getLocale());
        
        // setting a variable (in this case today's date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        Calendar cal = Calendar.getInstance();
        webContext.setVariable("today", dateFormat.format(cal.getTime()));
    
        // render the web context using a named template, sending it to the  response's writer
        String path = request.getPathInfo();
        CurrencyCalculator calculator;
        calculator = new CurrencyCalculator();
        webContext.setVariable("currencies", calculator.availableCurrencies());
        switch (path) {
            case "/currencies":
                /*
                TASK 1: print the currencies available by creating a new template
                and sending the response there.
                */
                
                templateEngine.process("currencies", webContext,response.getWriter());
                break;
            case "/convert":
                BigDecimal ans = calculator.convert(BigDecimal.valueOf(Double.parseDouble
                (request.getParameter("amount"))),request.getParameter("in"),
                request.getParameter("out"));
                webContext.setVariable("ans",ans);
                
                templateEngine.process("convert", webContext, response.getWriter());
                /*
                TASK 2: change the form on the home template to put the avilable
                currencies as dropdowns. Convert using the calculator
                and write the result into a template. 
                */
                break;
            default:
                templateEngine.process("home", webContext, response.getWriter());
                break;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
