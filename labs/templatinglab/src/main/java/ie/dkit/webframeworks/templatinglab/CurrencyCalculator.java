package ie.dkit.webframeworks.templatinglab;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CurrencyCalculator {
    
    private final Map<String,BigDecimal> currencies;
    
    public CurrencyCalculator() {
        currencies = new HashMap<>();
        currencies.put("EUR", new BigDecimal("1.00"));
        currencies.put("USD", new BigDecimal("1.25"));
        currencies.put("GBP", new BigDecimal("0.75"));
        currencies.put("DDK", new BigDecimal("10.00"));
    }
    
    public BigDecimal convert(BigDecimal amount, String fromCurrency, String toCurrency) {
        if ( fromCurrency.equals(toCurrency)) {
            return amount;
        }
        return amount.divide(currencies.get(fromCurrency)).multiply(currencies.get(toCurrency));
    }
    
    public Set<String> availableCurrencies() {
        return currencies.keySet();
    }
}
