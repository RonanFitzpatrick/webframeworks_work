package ie.dkit.webframeworks.templatinglab;

import java.math.BigDecimal;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class CurrencyCalculatorTest {
    
    private CurrencyCalculator calculator;
    
    @Before
    public void setup() {
        calculator = new CurrencyCalculator();
    }

    @After
    public void teardown() {
        calculator = null;
    }
    
    @Test
    public void testSameCurrency() {
        BigDecimal amount = new BigDecimal("1.00");
        assertEquals(amount, calculator.convert(amount, "EUR", "EUR"));
    }
    
    @Test
    public void testKnownConversion() {
        BigDecimal euro = new BigDecimal("1.00");
        BigDecimal dollar = new BigDecimal("1.25");
        assertEquals(dollar, calculator.convert(euro, "EUR", "USD"));
    }
    
}
