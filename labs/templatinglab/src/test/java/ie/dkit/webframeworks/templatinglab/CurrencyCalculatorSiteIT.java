package ie.dkit.webframeworks.templatinglab;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Assert;
import org.junit.Test;

public class CurrencyCalculatorSiteIT {
    
    WebClient webClient = new WebClient();
    
    protected String urlStem;
    public CurrencyCalculatorSiteIT() {
        urlStem = "http://localhost:8180/templatinglab/";
    }
    
    private String fullUrlForPath(String path) {
        return urlStem + path;
    }
    
    @Test
    public void currencyListWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("currencies"));
        Assert.assertTrue(page.getBody().asText().contains("EUR"));
        Assert.assertTrue(page.getBody().asText().contains("USD"));
        Assert.assertTrue(page.getBody().asText().contains("GBP"));
    }
    
    @Test
    public void conversionPageWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("convert?amount=10.00&from=EUR&to=USD"));
        Assert.assertTrue(page.getBody().asText().contains("12.50"));
    }
    
}
