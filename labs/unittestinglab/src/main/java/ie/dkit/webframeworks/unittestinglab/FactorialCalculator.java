package ie.dkit.webframeworks.unittestinglab;

public class FactorialCalculator {
    
    public int factorial(int n) throws ArithmeticException 
    {
        if(n < 0)
        {
            throw new ArithmeticException("negative number");
        }
        int answer = 1;
        for (int i=1;i<=n; i++)
        {
         answer = answer*i;
        }
        return answer;
    }
        
    }
    

