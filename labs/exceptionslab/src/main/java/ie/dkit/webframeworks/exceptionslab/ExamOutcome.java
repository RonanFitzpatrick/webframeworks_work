package ie.dkit.webframeworks.exceptionslab;

public enum ExamOutcome {
    PASS,
    FAIL
}
