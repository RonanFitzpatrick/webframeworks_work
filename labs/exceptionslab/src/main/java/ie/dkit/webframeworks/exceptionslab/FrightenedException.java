package ie.dkit.webframeworks.exceptionslab;

/**
 *
 * @author Peadar Grant
 */
public class FrightenedException extends Exception {

    /**
     * Creates a new instance of <code>FrightenedException</code> without detail
     * message.
     */
    public FrightenedException() {
    }

    /**
     * Constructs an instance of <code>FrightenedException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public FrightenedException(String msg) {
        super(msg);
    }
}
