package ie.dkit.webframeworks.exceptionslab;

/**
 *Pet Animal class
 * 
 * DO NOT MODIFY THIS FILE DURING THE EXERCISE.
 */
public class PetAnimal {
    
    public void checkin(int hour) throws HungryException, ThirstyException, FrightenedException {
        if ( 6 == hour || 14 == hour || 20 == hour ) {
            throw new HungryException();
        } else if ( 15 == hour ) {
            throw new ThirstyException();
        } else if ( 23 == hour ) {
            throw new FrightenedException();
        }
    }
    
}
