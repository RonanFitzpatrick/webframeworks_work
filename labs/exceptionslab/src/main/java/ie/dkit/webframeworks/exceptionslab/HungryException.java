package ie.dkit.webframeworks.exceptionslab;

/**
 *
 * @author Peadar Grant
 */
public class HungryException extends Exception {

    /**
     * Creates a new instance of <code>HungryException</code> without detail
     * message.
     */
    public HungryException() {
    }

    /**
     * Constructs an instance of <code>HungryException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public HungryException(String msg) {
        super(msg);
    }
}
