package ie.dkit.webframeworks.exceptionslab;

/**
 *
 * @author Peadar Grant
 */
public class ThirstyException extends Exception {

    /**
     * Creates a new instance of <code>ThirstyException</code> without detail
     * message.
     */
    public ThirstyException() {
    }

    /**
     * Constructs an instance of <code>ThirstyException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ThirstyException(String msg) {
        super(msg);
    }
}
