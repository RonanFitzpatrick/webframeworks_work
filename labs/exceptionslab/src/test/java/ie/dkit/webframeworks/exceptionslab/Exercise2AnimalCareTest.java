package ie.dkit.webframeworks.exceptionslab;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Peadar Grant
 */
public class Exercise2AnimalCareTest {
    
    Exercise2AnimalCare exercise;
    
    @Before
    public void setup() {
        exercise = new Exercise2AnimalCare();
    }
    
    @After
    public void teardown() {
        exercise = null;
    }
    
    @Test
    public void okMessageShouldBeReturned() throws Exception {
        assertEquals("everything is OK", exercise.statusAt(1));
    }
    
    @Test
    public void hungryMessageShouldBeReturned() throws Exception {
        assertEquals("the animal is hungry", exercise.statusAt(6));
    }
    
    @Test
    public void thirstyMessageShouldBeReturned() throws Exception {
        assertEquals("the animal needs a drink", exercise.statusAt(15));
    }
    
    @Test(expected=FrightenedException.class)
    public void frightenedExceptionShouldBeThrownUpwards() throws Exception {
        exercise.statusAt(23);
    }
    
}
