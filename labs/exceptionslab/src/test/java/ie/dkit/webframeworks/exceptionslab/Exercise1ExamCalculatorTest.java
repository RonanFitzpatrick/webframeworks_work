package ie.dkit.webframeworks.exceptionslab;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class Exercise1ExamCalculatorTest {

    private static final int PASSMARK=40;
    private static final int MAXIMUM_MARK=100;
    private static final int MINIMUM_MARK=0;
    private Exercise1ExamCalculator calculator;
    
    @Before
    public void setup() {
        calculator = new Exercise1ExamCalculator();
    }
    
    @Test
    public void markBelowPassmarkFails() {
        assertEquals(ExamOutcome.FAIL, calculator.outcomeForMark(PASSMARK-1));
    }
    
    @Test
    public void passmarkPasses() {
        assertEquals(ExamOutcome.PASS, calculator.outcomeForMark(PASSMARK));
    }
    
    @Test
    public void markAbovePassmarkPasses() {
        assertEquals(ExamOutcome.PASS, calculator.outcomeForMark(PASSMARK+1));
    }
    
    @Test
    public void maximumMarkPasses() {
        assertEquals(ExamOutcome.PASS, calculator.outcomeForMark(MAXIMUM_MARK));
    }
    
    @Test
    public void minimumMarkFails() {
        assertEquals(ExamOutcome.FAIL, calculator.outcomeForMark(MINIMUM_MARK));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void outOfRangePositiveThrowsException() {
        calculator.outcomeForMark(MAXIMUM_MARK+1);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void outOfRangeNegativeThrowsException() {
        calculator.outcomeForMark(MINIMUM_MARK-1);
    }
}
