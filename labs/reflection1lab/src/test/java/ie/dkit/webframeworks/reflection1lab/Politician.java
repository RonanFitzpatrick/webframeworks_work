package ie.dkit.webframeworks.reflection1lab;

public class Politician extends Person {
    
    private String party;
    
    public Politician() {
        super();
        this.party = "Tea party";
    }
    
    public Politician(String firstname, String surname, String party) {
        super(firstname, surname);
        this.party = party;
    }
    
    @Override
    public void setFirstnameAndSurname() {
        this.firstname = "John";
        this.surname = "Doe";
    }
    
    @Override
    public void setFirstnameAndSurname(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname; 
    }
    
    @Override
    public String getFullname() {
        return String.format("%s, %s (%s)", surname, firstname, party);
    }
    
}
