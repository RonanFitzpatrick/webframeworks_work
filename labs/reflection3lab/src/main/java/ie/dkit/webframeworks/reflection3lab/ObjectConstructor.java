package ie.dkit.webframeworks.reflection3lab;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ObjectConstructor
{

    public Object constructObjectWithParameters(
            String className,
            Object[] parameters)
            throws Exception
    {
        Object class_reference = null;
        Object created_object = null;
        class_reference = Class.forName(className).newInstance();
        ArrayList<Class> types = new ArrayList<>();
        for (int i = 0; i < parameters.length; i++)
        {
            
            types.add(parameters[i].getClass());
            if(types.get(i) == Integer.class)
            {
                
                types.add(i,int.class);
                types.remove(types.size()-1);
            }
            
           
            
            
        }
        System.out.println(types);
        created_object = class_reference.getClass()
                        .getConstructor(types.toArray(new Class[]
                         {})).newInstance((Object[]) parameters);

        return created_object;
    }

}
