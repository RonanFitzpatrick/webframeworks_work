package ie.dkit.webframeworks.reflection3lab;

import java.util.ArrayList;

public class MethodCaller {
    
    public Object callMethodWithParameters(
            Object object, 
            String methodName, 
            Object[] parameters) 
            throws Exception {
        
        ArrayList<Class> types = new ArrayList<>();
    for (int i = 0; i < parameters.length; i++)
        {
            
            types.add(parameters[i].getClass());
         if(types.get(i) == Integer.class)
            {
                
                types.add(i,int.class);
               
                types.remove(types.size()-1);
            }
         if(types.get(i) == Double.class)
            {
                
                types.add(i,double.class);
                
                types.remove(types.size()-1);
            }
        }
        
        
        return object.getClass().getMethod(methodName,types.toArray(new Class[]{})).invoke(object,parameters);
        
        
    }
    
}
