package ie.dkit.webframeworks.reflection3lab;

/*
NB: This is NOT a good example of how to factor a data-carrying class!
There's lots of bad practices here.
Can you find them?
*/

public class Person {
    
    private String firstname;
    private String surname;
    private int salary = 0;
    private int bonus = 0;
    private String companyCar = null;
    private String performanceComments = null;
    private double performanceRating = 0;
    
    public Person() {
        firstname = "John";
        surname = "Doe";
    }
    
    public Person(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
    }
    
    public Person(String firstname, String surname, int salary) {
        this.firstname = firstname;
        this.surname = surname; 
        this.salary = salary;
    }
    
    public void rename(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
    }
    
    public void rename(String fullname) {
        String[] components = fullname.split(", ");
        this.surname = components[0];
        this.firstname = components[1];
    }
    
    public void assignSalaryPackage(int salary) {
        this.salary = salary;
        this.bonus = 0;
        this.companyCar = null;
    }
    
    public void assignSalaryPackage(int salary, int bonus) {
        this.salary = salary;
        this.bonus = bonus;
        this.companyCar = null;
    }
    
    public void assignSalaryPackage(int salary, String companyCar) {
        this.salary = salary;
        this.bonus = 0;
        this.companyCar = companyCar;
    }
    
    public void assignSalaryPackage(int salary, int bonus, String companyCar) {
        this.salary = salary;
        this.bonus = bonus;
        this.companyCar = companyCar;
    }
    
    public void ratePerformance(double rating) {
        this.performanceRating = rating;
        this.performanceComments = null;
    }
    
    public void ratePerformance(double rating, String comments) {
        this.performanceRating = rating;
        this.performanceComments = comments;
    }
    
    @Override
    public String toString() {
        return String.format("%s, %s ( %d + %d, )", surname, firstname, salary, bonus, companyCar!=null?companyCar:"no car");
    }
    
}
