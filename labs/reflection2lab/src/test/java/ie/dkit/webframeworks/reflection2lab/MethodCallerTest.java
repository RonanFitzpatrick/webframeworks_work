package ie.dkit.webframeworks.reflection2lab;

import java.net.URL;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.mockito.Mockito;

public class MethodCallerTest {
    
    MethodCaller methodCaller;
    
    @Before
    public void setup() {
        methodCaller = new MethodCaller();
    }
    
    @After
    public void tearDown() {
        methodCaller = null;
    }

    @Test
    public void callMethodByName1() throws Exception {
        Person p = new Person();
        assertEquals(p.fullinfo(), methodCaller.callMethod(p, "fullinfo") );
    }
    
    @Test
    public void callMethodByName2() throws Exception {
        URL url = new URL("http://www.google.ie/info");
        assertEquals(url.getHost(), methodCaller.callMethod(url, "getHost"));
    }
    
    @Test
    public void callMethodWith1Parameter() throws Exception {
        Person mockPerson = Mockito.mock(Person.class);
        methodCaller.callMethodWithParameters(mockPerson, "setFullname", new Object[] { "Darcy, Gordon" } );
        Mockito.verify(mockPerson).setFullname("Darcy, Gordon");
    }
    
    @Test
    @Ignore
    public void callMethodWithMultipleParameters() throws Exception {
        Person mockPerson = Mockito.mock(Person.class);
        methodCaller.callMethodWithParameters(mockPerson, "set", new Object[] { "Bob", "Jones", 40 } );
        Mockito.verify(mockPerson).set("Bob", "Jones", 40);
    }
}

