package ie.dkit.webframeworks.reflection2lab;

public class Person {
    
    private String firstname;
    private String surname;
    private int age;
    
    public Person() {
        this.firstname = "John";
        this.surname = "Doe";
        age = 0;
    }
    
    public Person(String firstname, String surname, int age) {
        this.firstname = firstname;
        this.surname = surname;
        this.age = age;
    }
    
    public String fullname() {
        return String.format("%s, %s", this.surname, this.firstname);
    }
    
    public String fullinfo() {
        return String.format("%s (%s)", this.fullname(), this.age);
    }
    
    public void setFullname(String fullname) {
        String[] parts = fullname.split(", ");
        this.firstname = parts[0];
        this.surname = parts[1];
    }
    
    public void set(String firstname, String surname, int age) {
        this.firstname = firstname; 
        this.surname = surname; 
        this.age = age; 
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname; 
    }
}
