package ie.dkit.webframeworks.reflection2lab;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class MethodsLister {
    
    public ArrayList<String> allMethodsSupportedFor(Object object) {
        
        ArrayList<Method> list = new ArrayList();
        ArrayList<String> names = new ArrayList();
        list = new ArrayList(Arrays.asList(object.getClass().getMethods()));
        for(Method m : list)
        {
            names.add(m.getName());
        }
        return names;    
    }
    
    public ArrayList<String> declaredMethodsSupportedFor(Object object) {
        
        // Given an object, return an ArrayList of the method names that that
        // object has, excluding those declared in a superclass.
        // There are at least two ways to do this, so pick the easier one. 
        ArrayList<Method> list = new ArrayList();
        ArrayList<String> names = new ArrayList();
        list = new ArrayList(Arrays.asList(object.getClass().getDeclaredMethods()));
        for(Method m : list)
        {
            names.add(m.getName());
        }
        return names;
        
    }
    
    
    
}
