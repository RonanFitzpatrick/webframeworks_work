package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;

/**
 *
 * @author ronan
 */
public class UrlParserImpl implements UrlParser
{

    @Override
    public ParsedUrl parse(String url) throws MalformedURLException
    {
        String reg = "([a-zA-Z1-9]+)://([a-zA-Z1-9\\.]+)(/([A-Za-z]+[/]?)?)?";
        
        if(RegexUtility.matches(url,reg))
        {
       return new Url(url);
        }
        else
        {
            throw new MalformedURLException();
        }
    }
    
}
