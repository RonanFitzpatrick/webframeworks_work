package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ronan
 */
public class Url implements ParsedUrl 
{
    private String protocol =  null;
    private String host = null;
    private String path = null;
    private String parameter = null;

    public Url(String url)
    {
        String reg = "([a-zA-Z1-9]+)://([a-zA-Z1-9\\.]+)(/([A-Za-z]+[/]?)?)?";
        
        
        if(RegexUtility.matches(url,reg))
        {
            
            
            this.host = RegexUtility.getFirst(url, reg, 2);
            
            
            this.path = RegexUtility.getFirst(url, reg, 4); 
            
            this.protocol = RegexUtility.getFirst(url, reg, 1);
            
            
        }
        
        
    }
    

    @Override
    public String getProtocol() 
    {      
        return protocol;
    }

    @Override
    public String getHost () 
    {      
        return this.host;
    }

    @Override
    public String getPath() 
    {
         return this.path;
    }

    @Override
    public String getParameter(String parameterName)
    {
        return parameter;
    }
    
}
