package ie.dkit.webframeworks.javaweblab;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

public class CalculatorJSPIT {
    
    WebClient webClient = new WebClient();
    
    protected String urlStem;
    public CalculatorJSPIT() {
        urlStem = "http://localhost:8180/javaweblab/calculator.jsp?";
    }
    
    private String fullUrlForPath(String path) {
        return urlStem + path;
    }
    
    @Test
    public void addWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=add"));
        Assert.assertTrue(page.getBody().asText().contains("50"));
    }
    
    @Test
    public void subtractWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=subtract"));
        Assert.assertTrue(page.getBody().asText().contains("10"));
    }
    
    @Test
    public void multiplyWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=multiply"));
        Assert.assertTrue(page.getBody().asText().contains("600"));
    }
    
    @Test
    public void divideWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=3000&y=15&action=divide"));
        Assert.assertTrue(page.getBody().asText().contains("200"));
    }
    
}
