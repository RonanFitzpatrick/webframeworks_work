package ie.dkit.webframeworks.javaweblab;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Assert;
import org.junit.Test;

public class IndexJSPIT {
    
    private static String urlStem = "http://localhost:8180/javaweblab";
    
    private String fullUrlForPath(String path) {
        return urlStem + path;
    }
    
    @Test
    public void homePage() throws Exception {
        WebClient webClient = new WebClient();
        HtmlPage page = webClient.getPage(fullUrlForPath("/"));
        Assert.assertEquals("JSP Page", page.getTitleText());
    }
    
}
