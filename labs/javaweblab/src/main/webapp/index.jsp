<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p><a href="DemoServlet">Demo servlet</a></p>
        <p><a href="demo.jsp">Demo JSP</a></p>
        <h2>Calculator</h2>
        <form method="GET" action="calculator.jsp">
            <p>x = <input type="text" name="x" value="0" /></p>
            <p>y = <input type="text" name="y" value="0" /></p>
            <p>
                <select name="action">
                    <option value="add">+</option>
                    <option value="subtract">-</option>
                    <option value="multiply">*</option>
                    <option value="divide">/</option>
                </select>
            </p>
            <p><input type="submit" value="=" /></p>
        </form>
    </body>
</html>
