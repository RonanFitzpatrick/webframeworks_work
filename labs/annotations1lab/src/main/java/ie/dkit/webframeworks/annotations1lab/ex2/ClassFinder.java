package ie.dkit.webframeworks.annotations1lab.ex2;

import java.util.HashSet;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;

public class ClassFinder {
    
    Set<String> classNamesInPackageExtending(String packageName, Class extendedType) 
    {
        // use the Reflections library:
        // https://github.com/ronmamo/reflections
        // to get the class names in the package.
        
        Reflections reflections = new Reflections(packageName);

        Set <String> names = new HashSet();
        Set<Class<?>> types = reflections.getSubTypesOf(extendedType);
        for(Class s :types)
        {
            names.add(s.getSimpleName());
        }
        return names;
    }
    
}
