package ie.dkit.webframeworks.annotations1lab.ex1;

import ie.dkit.webframeworks.annotations1lab.ex1.Confidential;
import ie.dkit.webframeworks.annotations1lab.ex1.Person;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

public class Ex1Test {
    
    private Person person;
    
    @Before
    public void setup() {
        person = new Person();
    }

    @Test
    public void salaryFieldShouldBeAnnotatedConfidentialAtRuntime() throws NoSuchFieldException {
        assertNotNull(person.getClass().getDeclaredField("salary").getAnnotation(Confidential.class));
    }
    
    @Test
    public void socialSecurityNumberFieldShouldBeAnnotatedConfidentialAtRuntime() throws NoSuchFieldException {
        assertNotNull(person.getClass().getDeclaredField("socialSecurityNumber").getAnnotation(Confidential.class));
    }

        
    @Test
    public void confidentialAnnotationShouldOnlyApplyToFields() {
        ElementType[] types = Confidential.class.getAnnotation(Target.class).value();
        assertEquals(1, types.length);
        assertEquals(ElementType.FIELD, types[0]);
    }
    
    @After
    public void teardown() {
        person = null;
    }
}
